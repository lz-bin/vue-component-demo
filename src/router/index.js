import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        redirect: '/dashboard',
    },
    {
        path: '/dashboard',
        component: () => import('@/views/dashboard/index'),
    },
    {
        path: '/test',
        component: () => import('@/views/test/index'),
    },
]

const router = new VueRouter({
    routes
})

export default router